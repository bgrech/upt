"""Restraint output filtering."""
import re


class RestraintHost:
    """Identify host/recipe_id in restraint output."""

    rgx_host = re.compile(
        r'^Connecting to host: (?:(?:[^\n]+)@){0,1}(.*?)(?::[0-9]+){0,1}, '
        r'recipe id:([0-9]+)$')
    rgx_host_line = re.compile(r'^\s*\[(.*?)\s*\] ([^\n]+)', re.MULTILINE)
    rgx_state_change = re.compile(r'T:\s*([0-9]+)\s*\[(.*)\]\s+(.*?)(?:: (.*?)){0,1}$')
    rgx_disconnect = re.compile(r'^Disconnected\.\. delaying 60 seconds\. Retry '
                                r'([0-9]+)/([0-9]+)\.$', re.IGNORECASE)

    def __init__(self, hostname, recipe_id):
        """Create the object."""
        self.hostname = hostname
        self.recipe_id = int(recipe_id)
        self.lines = []

    @classmethod
    def from_line(cls, line):
        """Create object from shell command line."""
        rgx = re.compile(r'([0-9]+)=.*?@(.*?)\s')
        return [RestraintHost(hostname, recipe_id) for
                recipe_id, hostname in rgx.findall(line)]

    @classmethod
    def in_list(cls, shorthand, lst):
        """Check if object is in lst, search by shortened (restraint name)."""
        for i, item in enumerate(lst):
            if item.hostname.startswith(shorthand):
                return i
        return None
