"""Create kcidb data."""
from copy import deepcopy
from datetime import timedelta
import json
import os
import pathlib
import typing

from cki_lib.config_tree import merge_dicts
from cki_lib.kcidb.validate import validate_kcidb
from cki_lib.misc import get_env_bool
from cki_lib.misc import now_tz_utc
from cki_lib.session import get_session
from dateutil.parser import parse as date_parse

from . import misc

SESSION = get_session('upt.upt.restraint.wrapper.kcidb_adapter')


def _is_empty(value):
    """Check if value is not empty."""
    return (
        value is None or
        value == [] or
        value == {}
    )


class RestraintStandaloneTest:
    # pylint: disable=too-many-instance-attributes
    """KCIDB test object adapter for test result from restraint client in standalone mode."""

    def __init__(
        self, adapter, task_result, *, original_task_id=None, testplan=False, rerun_index=1
    ):
        # pylint: disable=too-many-arguments
        """Create the object.

        Arguments:
            adapter          - kcidbtestadapter
            task_result      - TaskResult, a test result from restraint runner wrapper around
                               restraint client
            original_task_id - int, a task_id that matches order of the test in testplan
            testplan         - bool, if True, then this is a test that hasn't run yet, if False,
                               it is an incremental result
            rerun_index      - int, how many times the test was retried
        """
        self.adapter = adapter
        self.task_result = task_result
        self.original_task_id = original_task_id or task_result.task_id
        self.rerun_index = rerun_index

        self.testplan = testplan

        # Compute how long the test ran. In seconds. If computation is 0, remove this
        duration = int((now_tz_utc() - date_parse(task_result.start_time)).total_seconds()) \
            if task_result.start_time else 0
        self.duration = duration or None

        # Start time as ISO.
        self.start_time = task_result.start_time

        # Finish time as ISO.
        self.finish_time = (
            (date_parse(self.task_result.start_time) + timedelta(seconds=self.duration)).isoformat()
            if self.task_result.start_time and self.duration else None
        )

    @property
    def output_files(self):
        """Get a list of test outputs: logs, dumps, etc."""
        return [self._prepare_output_file(file) for file in self.task_result.output_files]

    @staticmethod
    def _prepare_output_file(output_file: pathlib.Path) -> typing.Dict[str, str]:
        """Prepare output_file and return it formatted in the KCIDB format."""
        fixed_file = misc.fix_file_name_encoded_as_html(output_file)
        url = misc.convert_path_to_link(fixed_file)
        return {'name': fixed_file.name, 'url': url}

    def render(self):
        """Create dict object with data to dump."""
        data = self._render().copy()

        # Don't dump misc field for testplan.
        if not self.testplan:
            data.setdefault('misc', {})['provenance'] = self.provenance_data
        data['origin'] = 'redhat'
        return {
            key: value for key, value in data.items() if not _is_empty(value)
        }

    @property
    def provenance_data(self):
        """Return information about systems involved in this test execution."""
        provenance = []
        if get_env_bool('GITLAB_CI', False):
            provenance.append(
                {
                    'function': 'coordinator',
                    'url': os.environ['CI_JOB_URL'],
                    'service_name': 'gitlab',
                }
            )
        if os.environ.get('BEAKER_URL'):
            provenance.append(
                {
                    'function': 'provisioner',
                    'url': f'{os.environ["BEAKER_URL"]}/recipes/{self.task_result.recipe_id}',
                    'service_name': 'beaker',
                }
            )

        return provenance

    def _render(self):
        """Return the KCIDB compatible data."""
        task_result = self.task_result

        # Copy the test data from KCIDB to avoid overwriting the file from
        # multiple upt threads (fun!)
        test_data = deepcopy(next(
            test_data for test_data in self.adapter.tests if test_data['id'] ==
            f'{self.adapter.build["id"]}_{self.adapter.test_prefix}_{task_result.cki_id}'
        ))
        test_data['start_time'] = task_result.start_time
        test_data['output_files'] = []

        if not self.testplan:
            merge_dicts(test_data, {'misc': self.misc})
            # NOTE: We should fill in the environment the test ran in. E.g. a
            # host, a set of hosts, or a lab; amount of memory/storage/CPUs,
            # for each host; process environment variables, etc.
            test_data['environment'] = {'comment': self.task_result.host.hostname}
            test_data['duration'] = self.duration
            test_data['output_files'] = self.output_files
            # Add log_url if the test contains a `taskout.log` log
            for log in self.output_files:
                if log['name'] == 'taskout.log':
                    test_data['log_url'] = log['url']
                    break

            # Remove results if exist previously
            if 'results' in test_data['misc']:
                del test_data['misc']['results']
            # Add results if exist
            if self.task_result.results:
                test_data['misc']['results'] = self.get_results(test_data['id'])

        # "ERROR", "FAIL", "PASS", "DONE", "SKIP", "MISS"
        test_data['status'] = task_result.kcidb_status

        # We only specify kcidb path attribute if we know it, it can be empty
        if task_result.universal_id:
            test_data['path'] = task_result.universal_id.replace('/', '.')

        return {
            key: value for key, value in test_data.items() if not _is_empty(value)
        }

    @property
    def misc(self):
        """Miscellaneous extra data about the test."""
        data = {
            'fetch_url': self.task_result.fetch_url,
            'beaker': {
                'task_id': self.original_task_id,
                'recipe_id': self.task_result.recipe_id
            },
            'maintainers': self.task_result.test_maintainers,
            'rerun_index': self.rerun_index
        }
        if self.task_result.cki_polarion_id:
            data['polarion_id'] = self.task_result.cki_polarion_id
        if self.finish_time:
            data['beaker']['finish_time'] = self.finish_time

        return data

    def get_results(self, test_id):
        """Add subtests results."""
        return [self.get_result(result, test_id) for result in self.task_result.results]

    def get_result(self, result, test_id):
        """Get info for a subtest result."""
        return {
            'id': f'{test_id}.{result.result_id}',
            'name': result.name,  # deprecated
            'comment': result.name,
            'status': result.status,
            'output_files': [self._prepare_output_file(file) for file in result.output_files]
        }


class KCIDBTestAdapter:
    # pylint: disable=too-many-instance-attributes,too-few-public-methods
    """Dump restraint runner test results as kcidb data files."""

    def __init__(self, **kwargs):
        """Create the object."""
        self.kwargs = kwargs
        self.instance_no = kwargs['instance_no']
        # Common project output directory
        self.output = kwargs['output']
        self.kcidb_data = kwargs['kcidb_data']

        self.job_id = os.environ['CI_JOB_ID']
        self.instance_url = os.environ['CI_SERVER_URL']
        self.project_id = os.environ['CI_PROJECT_ID']
        self.path_with_namespace = os.environ['CI_PROJECT_PATH']

        self.checkout = self.kcidb_data.get_checkout(os.environ['KCIDB_CHECKOUT_ID'])
        self.build = self.kcidb_data.get_build(os.environ['KCIDB_BUILD_ID'])
        self.tests = self.kcidb_data.data.get('tests')
        self.test_prefix = os.environ.get('KCIDB_TEST_PREFIX', 'upt')

    @staticmethod
    def dump(kcidb_data, to_where):
        """Dump kcidb_data to a file."""
        validate_kcidb(kcidb_data)

        with open(to_where, 'w', encoding='utf-8') as fhandle:
            json.dump(kcidb_data, fhandle, indent=4)
