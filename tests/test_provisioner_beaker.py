"""Test cases for Beaker provisioner module."""
import itertools
import os
import pathlib
import subprocess
import unittest
from unittest import mock
import xml.etree.ElementTree as ET

from tests.const import ASSETS_DIR
from upt.cmd_legacy import convert_xml
from upt.logger import COLORS
from upt.misc import RET
from upt.plumbing.end_conditions import ProvEndConds
from upt.plumbing.format import ProvisionData
from upt.plumbing.objects import ResourceGroup
import upt.provisioners.beaker as beaker
from upt.restraint.file import RestraintJob
from upt.restraint.file import RestraintRecipe


class TestJobSubmissions(unittest.TestCase):
    """Test cases for submit_job and clone functions."""

    def setUp(self):
        self.bkr_output_500 = ('something went wrong\nreally wrong ok, '
                               '500 Internal Server Error\nthis is a trace\n')
        self.bkr_output_bad = 'ooops'
        self.bkr_output_good = 'J:123'

    @mock.patch('cki_lib.misc.safe_popen')
    def test_retry_submit(self, mock_safe_popen):
        """Ensure submission is retried on 500 errors."""
        mock_safe_popen.side_effect = [(self.bkr_output_500, 'no error', 1),
                                       (self.bkr_output_good, 'ok', 0)]
        returned_values = beaker.submit_job('something')
        self.assertEqual(returned_values, ['J:123'])

    @mock.patch('cki_lib.misc.safe_popen')
    def test_retry_clone(self, mock_safe_popen):
        """ensure cloning is retried on 500 errors."""
        mock_safe_popen.side_effect = [(self.bkr_output_500, 'no error', 1),
                                       (self.bkr_output_good, 'ok', 0)]
        returned_values = beaker.clone('something')
        self.assertEqual(returned_values, ['J:123'])

    @mock.patch('cki_lib.misc.safe_popen')
    def test_no_retry_clone(self, mock_safe_popen):
        """ensure cloning is not retried on non-500 errors."""
        mock_safe_popen.return_value = (self.bkr_output_bad, 'no error', 1)
        with self.assertRaises(RuntimeError):
            beaker.clone('something')

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('builtins.print')
    def test_submit_job(self, mock_print, mock_safe_popen):
        """Ensure submit_job works."""
        stdout = 'J:1234 J:5678'
        mock_safe_popen.return_value = (stdout, '', 0)
        resource_ids = beaker.submit_job('whatever')

        self.assertEqual(resource_ids, stdout.split(' '))

        mock_print.assert_called_with(f'Submitted: {" ".join(resource_ids)}')

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('builtins.print')
    @mock.patch('upt.logger.LOGGER.error')
    def test_submit_job_err(self, mock_error, mock_print, mock_safe_popen):
        """Ensure submit_job raises exception and prints stdout on non-zero retcode."""

        mock_safe_popen.return_value = ('stdout', '', 1)

        beaker.submit_job('whatever')

        mock_print.assert_called_with('stdout')
        mock_error.assert_called_with('submitting job failed!')

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('builtins.print', mock.Mock())
    @mock.patch('upt.provisioners.beaker.sys.stderr.write')
    def test_submit_job_supress(self, mock_write, mock_safe_popen):
        """Ensure submit_job raises exception and prints stdout on non-zero retcode."""

        mock_safe_popen.return_value = ('',  'somefail\nWARNING: job xml validation failed: '
                                             'Element task has extra content', 0)
        beaker.submit_job('whatever')

        mock_write.assert_called_with('somefail\n')

    @mock.patch('cki_lib.misc.safe_popen')
    def test_clone(self, mock_safe_popen):
        """Ensure clone works."""
        stdout = 'J:1234 J:5678'
        mock_safe_popen.return_value = (stdout, '', 0)
        resource_ids = beaker.clone('whatever')

        self.assertEqual(resource_ids, stdout.split(' '))

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('builtins.print')
    def test_clone_err(self, mock_print, mock_safe_popen):
        """Ensure clone raises exception and prints stdout on non-zero retcode."""

        mock_safe_popen.return_value = ('stdout', '', 1)
        with self.assertRaises(RuntimeError):
            beaker.clone('whatever')

        mock_print.assert_called_with('stdout')


class TestCancel(unittest.TestCase):
    """Test cases for the cancel function."""

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('builtins.print')
    def test_cancel(self, mock_print, mock_safe_popen):
        # pylint: disable=no-self-use
        """Ensure cancel works."""

        mock_safe_popen.return_value = ('stdout', '', 0)
        beaker.cancel('whatever')

        mock_print.assert_called_with('stdout')

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('builtins.print')
    @mock.patch('upt.logger.LOGGER.printc')
    def test_cancel_raise_on_error(self, mock_printc, mock_print, mock_safe_popen):
        """Ensure cancel works."""

        mock_safe_popen.return_value = ('stdout', '', 1)
        beaker.cancel('whatever')
        mock_printc.assert_called_with('Failed to release resources(s)', color=COLORS.RED)
        mock_print.assert_called_with('stdout')


class TestBeaker(unittest.TestCase):
    # pylint: disable=too-many-public-methods
    """Test cases for Beaker module."""

    def setUp(self) -> None:
        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')

        self.xmlpath = pathlib.Path(ASSETS_DIR, 'beaker_xml')
        self.xml = self.xmlpath.read_text()

        self.noop = lambda x: x
        self.noop_reprovision = lambda: None

    def test_exclude_hosts(self):
        """Ensure exclude_hosts works."""
        prov_data = ProvisionData.deserialize_file(self.req_asset)
        prov_data.get_provisioner('beaker').exclude_hosts(['hostname1', 'hostname2'])

        self.assertIn('<hostname op="!=" value="hostname1"></hostname>',
                      prov_data.get_provisioner('beaker').rgs[0].recipeset.hosts[0].recipe_fill)
        self.assertIn('<hostname op="!=" value="hostname2"></hostname>',
                      prov_data.get_provisioner('beaker').rgs[0].recipeset.hosts[0].recipe_fill)

    def test_exclude_hosts_and(self):
        """Ensure exclude_hosts works with and element."""
        prov_data = ProvisionData.deserialize_file(self.req_asset)
        prov_data.get_provisioner('beaker').exclude_hosts(['hostname1', 'hostname2'])

        recipeset0 = prov_data.get_provisioner('beaker').rgs[0].recipeset

        self.assertIn('<hostname op="!=" value="hostname1"></hostname>',
                      recipeset0.hosts[0].recipe_fill)
        self.assertIn('<hostname op="!=" value="hostname2"></hostname>',
                      recipeset0.hosts[0].recipe_fill)

        # Ensure the hostRequires element isn't changed when hostRequires force= is used.
        self.assertEqual('<hostRequires force="def"></hostRequires>', ET.canonicalize(ET.tostring(
            ET.fromstring(recipeset0.hosts[1].recipe_fill).find('hostRequires'), encoding='unicode'
        )))
        self.assertIn('<hostname op="!=" value="hostname1"></hostname>',
                      recipeset0.hosts[2].recipe_fill)
        self.assertIn('<hostname op="!=" value="hostname2"></hostname>',
                      recipeset0.hosts[2].recipe_fill)

        # # Ensure hostname exclude isn't appended twice.
        print(f'output: {recipeset0.hosts[2].recipe_fill}')
        expect = (
            '<recipe ks_meta="re"><hostRequires><hostname op="!=" value="hostname2">'
            '</hostname><hostname op="!=" value="hostname1"></hostname></hostRequires></recipe>')
        self.assertEqual(expect, recipeset0.hosts[2].recipe_fill)

    @mock.patch('upt.provisioners.beaker.Beaker.release_resources', mock.Mock())
    @mock.patch('time.sleep')
    @mock.patch('builtins.print', mock.Mock())
    @mock.patch('upt.plumbing.end_conditions.ProvEndConds.update_provisioning_state', mock.Mock())
    @mock.patch('upt.logger.LOGGER.printc', mock.Mock())
    def test_wait(self, mock_sleep):
        """Ensure wait works."""
        bkr = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')

        def succeed_and_fail_intermittently(self, force_recheck, **kwargs):
            data = [False, True, False, False, True]
            succeed_and_fail_intermittently.i += 1
            return data[succeed_and_fail_intermittently.i]

        succeed_and_fail_intermittently.i = -1
        rg_not_done = ResourceGroup()
        rg_not_done.provisioning_done = False
        with mock.patch('upt.plumbing.end_conditions.ProvEndConds.evaluate',
                        succeed_and_fail_intermittently):
            cond = ProvEndConds(rg_not_done, lambda x: False, self.noop_reprovision, self.noop,
                                self.noop, 600)

            result = bkr.rgs[0].wait(cond, lambda x: None, lambda x: None,
                                     **{'max_aborted_count': 3})
            self.assertEqual(result, RET.PROVISIONING_FAILED)

            mock_sleep.assert_called()

    @mock.patch('upt.provisioners.beaker.Beaker.release_resources')
    @mock.patch('time.sleep')
    @mock.patch('builtins.print')
    @mock.patch('upt.plumbing.end_conditions.ProvEndConds.update_provisioning_state')
    def test_wait_retcode(self, mock_update, mock_print, mock_sleep, mock_release_resources):
        """Ensure wait works and passess retcode around."""
        bkr = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')

        def fake_evaluate(self, force_recheck, **kwargs):
            self.retcode = kwargs.get('desired_retcode')
            return True

        rg_not_done = ResourceGroup()
        rg_not_done.provisioning_done = False

        with mock.patch('upt.plumbing.end_conditions.ProvEndConds.evaluate', fake_evaluate):
            cond = ProvEndConds(rg_not_done, lambda x: False, self.noop_reprovision, self.noop,
                                self.noop, 600)

            result = bkr.rgs[0].wait(cond, lambda x: None, lambda x: None,
                                     **{'max_aborted_count': 3,
                                        'desired_retcode': RET.PROVISIONING_FAILED})
            self.assertEqual(result, RET.PROVISIONING_FAILED)

            result = bkr.rgs[0].wait(cond, lambda x: None, lambda x: None,
                                     **{'max_aborted_count': 3,
                                        'desired_retcode': RET.PROVISIONING_PASSED})
            self.assertEqual(result, RET.PROVISIONING_PASSED)

    def test_get_resource_ids(self):
        """Ensure get_resource_ids works."""
        prov_data = ProvisionData.deserialize_file(self.req_asset)

        self.assertEqual(prov_data.get_provisioner('beaker').get_resource_ids(), ['J:1234'])

    @mock.patch('subprocess.run')
    @mock.patch('upt.logger.LOGGER.error')
    def test_set_reservation_duration(self, mock_error, mock_run):
        """Ensure set_reservation_duration works."""
        beaker_instance = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')

        mock_run.return_value = mock.Mock(returncode=0, stdout='some output')
        beaker_instance.set_reservation_duration(beaker_instance.rgs[0].recipeset.hosts[0])
        mock_error.assert_not_called()

        mock_run.return_value = mock.Mock(returncode=1, stdout='some output')
        beaker_instance.set_reservation_duration(beaker_instance.rgs[0].recipeset.hosts[0])
        mock_error.assert_called()

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('builtins.print')
    @mock.patch('time.sleep', lambda *args: None)
    @mock.patch('logging.warning', mock.Mock())
    def test_get_bkr_results(self, mock_print, mock_safe_popen):
        """Ensure get_bkr_results works."""
        beaker_instance = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')

        mock_safe_popen.return_value = ('some output', '', 1)
        with self.assertRaises(RuntimeError):
            beaker_instance.get_bkr_results('1234')
            mock_print.assert_called_with('some output')

        mock_safe_popen.return_value = ('', '', 0)
        beaker_instance.get_bkr_results('1234')

    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('upt.provisioners.beaker.Beaker.find_host_object', mock.Mock())
    def test_get_dead_resource_info_ewd(self, mock_debug):
        """Ensure get_dead_resource_info works for EWD check."""
        restraint_job = RestraintJob.create_from_string(self.xml)
        result_recipesets = restraint_job.recipesets
        recipe_ids_dead = set()
        result_recipes = list(itertools.chain(*[recipeset.recipes for recipeset in
                                                result_recipesets]))

        beaker.Beaker.get_dead_resource_info([mock.Mock()], result_recipes, recipe_ids_dead,
                                             beaker.Beaker.check_for_ewd)
        self.assertEqual(recipe_ids_dead, {1})
        mock_debug.assert_called_with('#%s: found EWD in reservesys task!', '1')

    @mock.patch('upt.logger.LOGGER.printc')
    @mock.patch('upt.provisioners.beaker.Beaker.find_host_object', mock.Mock())
    def test_get_dead_resource_info_panic(self, mock_printc):
        """Ensure get_dead_resource_info works for panic check."""
        restraint_job = RestraintJob.create_from_string(self.xml)
        result_recipesets = restraint_job.recipesets
        recipe_ids_dead = set()
        result_recipes = list(itertools.chain(*[recipeset.recipes for recipeset in
                                                result_recipesets]))

        beaker.Beaker.get_dead_resource_info([mock.Mock()], result_recipes, recipe_ids_dead,
                                             beaker.Beaker.check_for_panic)
        mock_printc.assert_called_with('#1: KERNEL PANIC!', color=COLORS.RED)

        # Ids must be integer compatible.
        with self.assertRaises(ValueError):
            restraint_recipe = RestraintRecipe.create_from_scratch()
            restraint_recipe.id = 'blah'
            beaker.Beaker.get_dead_resource_info([mock.Mock()], [restraint_recipe], [], [])

    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('time.sleep')
    def test_quirk_wait(self, mock_sleep, mock_debug):
        """Ensure quirk_wait works."""
        beaker_instance = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        with mock.patch.object(beaker_instance, 'get_provisioning_state',
                               lambda *args: (None, [], None)):
            beaker_instance.quirk_wait(beaker_instance.rgs[0])

            mock_debug.assert_called_with('* Caught Beaker quirk; waiting %d...', 60)
            mock_sleep.assert_called_with(60)

    def test_check_beaker_quirk(self):
        """Ensure check_beaker_quirk works."""
        restraint_recipe = RestraintRecipe.create_from_string(
            '<recipe status="Aborted" result="New" />')
        self.assertTrue(beaker.Beaker.check_beaker_quirk([restraint_recipe]))

        restraint_recipe = RestraintRecipe.create_from_string(
            '<recipe status="Completed" result="New" />')
        self.assertFalse(beaker.Beaker.check_beaker_quirk([restraint_recipe]))

        restraint_recipe = RestraintRecipe.create_from_string(
            '<recipe status="Completed" result="New" >'
            '<task status="Completed" result="New" />'
            '<task status="Aborted" result="New" /></recipe>')
        self.assertTrue(beaker.Beaker.check_beaker_quirk([restraint_recipe]))

    @mock.patch('builtins.print')
    @mock.patch('upt.logger.LOGGER.warning', mock.Mock())
    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('upt.provisioners.beaker.Beaker.find_host_object', mock.Mock())
    def test_heartbeat_with_errors(self, mock_debug, mock_print):
        """Ensure heartbeat works."""
        # Convert xml in asssets
        kwargs = {'excluded_hosts': '', 'force_host_duration': False, 'provisioner': 'beaker',
                  'priority': 'high'}
        beaker_instance = convert_xml(self.xml, **kwargs).get_provisioner('beaker')
        restraint_job = RestraintJob.create_from_string(self.xml)
        # Set recipe_id for each host, which would normally be set by updating provisioning request.
        beaker_instance.rgs[0].recipeset.hosts[0].recipe_id = 1
        beaker_instance.rgs[0].recipeset.hosts[1].recipe_id = 2

        # Use a fake check_beaker_quirk function to pass on 2nd attempt.
        def fake_quirk(_):
            try:
                if fake_quirk.called:
                    return False
            except AttributeError:
                fake_quirk.called = True
            return True

        result_recipesets = restraint_job.recipesets
        recipe_ids_dead = set()
        result_recipes = list(itertools.chain(
            *[recipeset.recipes for recipeset in result_recipesets]
        ))
        with mock.patch.object(beaker_instance, 'get_provisioning_state',
                               lambda *args: (None, result_recipesets, None)):
            with mock.patch.object(beaker_instance, 'check_beaker_quirk', fake_quirk):
                with mock.patch.object(beaker_instance, 'quirk_wait', lambda x: (result_recipes)):
                    beaker_instance.heartbeat(beaker_instance.rgs[0], recipe_ids_dead)

        mock_debug.assert_called_with('#%s: found EWD in reservesys task!', '1')

    def test_is_provisioned(self):
        """Ensure is_provisioned calls get_provisioning_state."""
        beaker_instance = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        with mock.patch.object(beaker_instance, 'get_provisioning_state',
                               lambda *args: (True, None, None)):
            self.assertTrue(beaker_instance.is_provisioned([None]))

    @mock.patch('builtins.print')
    def test_warn_once_provisioning_issue(self, mock_print):
        """Ensure warn_once_provisioning_issue works for recipes."""
        beaker_instance = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        beaker_instance._warned_rsetids = set()
        beaker_instance.warn_once_provisioning_issue(1, [1, 2], 1, 'Warn', '')
        mock_print.assert_called_with('* RS:1 ([1, 2]): R:1 Warn.')

    @mock.patch('builtins.print')
    def test_warn_once_provisioning_issue_reservesys(self, mock_print):
        """Ensure warn_once_provisioning_issue works for reservesys tasks."""
        beaker_instance = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        beaker_instance._warned_rsetids = set()
        beaker_instance.warn_once_provisioning_issue(1, [1, 2], 1, None, 'Warn reservesys')
        mock_print.assert_called_with(
            '* RS:1 ([1, 2]): found reservesys task that Warn reservesys.')

    def test_warn_once_provisioning_issue_unhandled(self):
        """Ensure warn_once_provisioning_issue checks unhandled conditions."""
        beaker_instance = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        beaker_instance._warned_rsetids = set()

        with self.assertRaises(RuntimeError):
            beaker_instance.warn_once_provisioning_issue(0, [], 1, None, None)

    @mock.patch('builtins.print')
    def test_warn_once_provisioning_issue_no_print(self, mock_print):
        """Ensure warn_once_provisioning_issue doesn't print twice."""
        beaker_instance = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        beaker_instance._warned_rsetids = {1}
        beaker_instance.warn_once_provisioning_issue(1, [], 1, 'err', 'err')
        mock_print.assert_not_called()

    @mock.patch('upt.logger.LOGGER.warning')
    def test_delete_failed_hosts(self, mock_warn):
        """Ensure delete_failed_hosts works and leaves only recipes with ids."""
        kwargs = {'excluded_hosts': '', 'force_host_duration': False, 'provisioner': 'beaker',
                  'priority': 'normal'}
        beaker_instance = convert_xml(self.xml, **kwargs).get_provisioner('beaker')
        restraint_job = RestraintJob.create_from_string(
            beaker_instance.rgs[1].recipeset.restraint_xml
        )
        restraint_job.recipesets[0].recipes[0].id = '1'
        beaker_instance.rgs[1].recipeset.restraint_xml = str(restraint_job)
        beaker_instance.delete_failed_hosts(beaker_instance.rgs[1].recipeset)

        restraint_recipes = RestraintJob.create_from_string(
            beaker_instance.rgs[1].recipeset.restraint_xml
        ).recipesets[0].recipes
        self.assertEqual(len(restraint_recipes), 1)
        mock_warn.assert_called_with(
            '%s removed from XML, no fetch element!', '/distribution/reservesys')

    @mock.patch('upt.logger.LOGGER.warning', mock.Mock())
    @mock.patch('upt.logger.LOGGER.printc', mock.Mock())
    def test_update_provisioning_request(self):
        """Ensure update_provisioning_request works."""
        beaker_instance = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')

        with mock.patch.object(beaker_instance, 'get_bkr_results') as mock_results:
            mock_results.return_value = self.xml

            beaker_instance.update_provisioning_request(beaker_instance.rgs[0])

            self.assertIn('<recipe id="1"', beaker_instance.rgs[0].recipeset.restraint_xml)

    def test_resource_group2xml(self):
        """Ensure resource_group2xml works."""
        self.maxDiff = None
        beaker_instance = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        result = beaker_instance.resource_group2xml(beaker_instance.rgs[0])
        restraint_job = RestraintJob.create_from_string(result)
        self.assertTrue(len(restraint_job.recipesets) > 0)

    @mock.patch('upt.provisioners.beaker.submit_job')
    def test_provision(self, mock_submit):
        """Ensure provision works."""
        beaker_instance = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')

        mock_submit.side_effect = [['J:1234']]
        all_resource_ids = beaker_instance.provision(**{})

        self.assertEqual(all_resource_ids, ['J:1234'])

    def test_finalize_recipes(self):
        """Ensure _finalize_recipes works."""
        beaker_instance = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')

        # Use any valid xml to see that  reservesys is added.
        restraint_job = RestraintJob.create_from_string(
            beaker_instance.rgs[0].recipeset.restraint_xml
        )
        self.assertNotIn('/distribution/reservesys', str(restraint_job))
        result = beaker_instance._finalize_recipes(
            restraint_job,
            beaker_instance.rgs[0].recipeset
        )
        self.assertIn('/distribution/reservesys', result)

    def test_finalize_recipeset_priority(self):
        """Ensure priority is adjusted as needed in _finalize_recipeset."""
        beaker_instance = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')

        result = beaker_instance._finalize_recipeset(beaker_instance.rgs[0])
        self.assertIn('recipeSet priority="Normal"', result)

        # Change the priority
        beaker_instance.rgs[0].priority = 'High'

        result = beaker_instance._finalize_recipeset(beaker_instance.rgs[0])
        self.assertIn('recipeSet priority="High"', result)

    @mock.patch('upt.provisioners.beaker.cancel')
    @mock.patch('upt.provisioners.beaker.clone')
    @mock.patch('builtins.print')
    def test_reprovision_aborted(self, mock_print, mock_clone, mock_cancel):
        """Ensure reprovision_aborted works."""
        beaker_instance = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        beaker_instance.rgs[0].erred_rset_ids = {1}

        with mock.patch.object(beaker_instance, 'get_bkr_results') as fake_results:
            fake_results.return_value = pathlib.Path(self.req_asset).read_text()
            mock_clone.return_value = ['J:1234']

            beaker_instance.reprovision_aborted(beaker_instance.rgs[0])
            mock_print.assert_called_with('* Cloned RS:1 as J:1234')

    @mock.patch('upt.provisioners.beaker.cancel')
    def test_release_resources(self, mock_cancel):
        """Ensure release_resources works."""
        beaker_instance = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        beaker_instance.release_resources()
        mock_cancel.assert_called_with('J:1234')

    @mock.patch('builtins.print')
    def test_get_provisioning_state(self, mock_print):
        """Ensure get_provisioning_state works."""
        beaker_instance = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')

        with mock.patch.object(beaker_instance, 'get_bkr_results') as fake_results:
            # Force an aborted result to get some interesting data
            fake_results.return_value = self.xml.replace(
                'recipe id="5"',
                'recipe id="5" status="Aborted"'
            )
            provisioned, recipesets, erred_rset_ids = beaker_instance.get_provisioning_state(
                beaker_instance.rgs[0]
            )
            self.assertFalse(provisioned)
            self.assertEqual(len(recipesets), 3)
            self.assertEqual(erred_rset_ids, {2})

            mock_print.assert_any_call("* RS:2 (['2', '3', '4', '5']): R:5 Aborted.")

    @mock.patch('builtins.print', mock.Mock())
    @mock.patch('upt.logger.LOGGER.warning')
    @mock.patch('upt.logger.LOGGER.printc')
    def test_update_provisioning_request_inconsistency(self, mock_printc, mock_warning):
        """Ensure update_provisioning_request works.

        Test for inconsistency of new bkr-results and internal data.
        """
        beaker_instance = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')

        with mock.patch.object(beaker_instance, 'get_bkr_results') as fake_results:
            fake_results.return_value = self.xml
            beaker_instance.rgs[0].erred_rset_ids = {1}
            beaker_instance.update_provisioning_request(beaker_instance.rgs[0])
            assert mock_warning.call_count == 4

            beaker_instance.rgs[0].erred_rset_ids = set()
            beaker_instance.update_provisioning_request(beaker_instance.rgs[0])
            mock_printc.assert_any_call('Error: a system went down shortly after provisioning '
                                        'was completed!', color=COLORS.RED)

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('upt.logger.LOGGER.error')
    def test_get_links2logs_fail(self, mock_error, mock_safe_popen):
        """Ensure get_links2logs reports non-zero retcode."""
        mock_safe_popen.return_value = ('', '', 1)

        beaker.Beaker.get_links2logs(1234)

        mock_error.assert_called_with('bkr job-logs issue')

    @mock.patch('cki_lib.misc.safe_popen')
    def test_get_links2logs(self, mock_safe_popen):
        """Ensure get_links2logs works."""
        base_url = 'https://beaker.engineering.redhat.com/recipes/'
        will_be_in_result = [f'{base_url}/1234/logs/console.log']
        wont_be_in_result = [f'{base_url}/9999/logs/console.log/',
                             f'{base_url}/1234/logs/sys.log']
        mock_safe_popen.return_value = ('\n'.join(will_be_in_result + wont_be_in_result), '', 0)

        result = beaker.Beaker.get_links2logs(1234)

        mock_safe_popen.assert_called_with(['bkr', 'job-logs', 'R:1234'], stdout=subprocess.PIPE)
        self.assertEqual(will_be_in_result, result)
